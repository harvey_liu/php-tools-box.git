## PHP开发工具箱 v1.1.4

### 👤作者     
**Harvey**
### 📮邮箱
**lenziye@qq.com**  


### 工具库
- Hashids  加密ID库 
- PhpAnalysis 搜索分词
- Snowflake 雪花算法,一种分布式唯一ID生成算法
- 腾讯相关
  - WXBizDataCrypt 微信小程序用户加密数据的解密
  - TLSSigAPIv2 腾讯音视频通话签名
- ApiAuthService Api签名验证


### 安装
```shell
composer require harvey/php-tools-box:^1.1.1  --ignore-platform-reqs
```
    

## 工具用法

### Hashids
>是一个用于生成短小、可读性强的哈希字符串的开源库，可以将整数或数组转换为不可逆的字符串表示形式。它适用于许多场景，如隐藏敏感信息（比如数据库中的ID）、创建独特的URL短链等。

```php
$obj = new \Harvey\Toolsbox\library\Hashids\Hashids;
$result1 = $obj->encode('00123456');

echo '加密  ===> ';
var_dump($result1);

$result2 = $obj->decode($result1);

echo '解密  ===> ';
var_dump($result2);
```
---
### WXBizDataCrypt用法
```php
use Harvey\Toolsbox\library\Tencent\wxBizDataCrypt\WXBizDataCrypt;

$appid = 'wx4f4bc4dec97d474b';
$sessionKey = 'tiihtNczf5v6AKRyjwEUhQ==';
$encryptedData="CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZM
                QmRzooG2xrDcvSnxIMXFufNstNGTyaGS
                9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+
                3hVbJSRgv+4lGOETKUQz6OYStslQ142d
                NCuabNPGBzlooOmB231qMM85d2/fV6Ch
                evvXvQP8Hkue1poOFtnEtpyxVLW1zAo6
                /1Xx1COxFvrc2d7UL/lmHInNlxuacJXw
                u0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn
                /Hz7saL8xz+W//FRAUid1OksQaQx4CMs
                8LOddcQhULW4ucetDf96JcR3g0gfRK4P
                C7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB
                6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns
                /8wR2SiRS7MNACwTyrGvt9ts8p12PKFd
                lqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYV
                oKlaRv85IfVunYzO0IKXsyl7JCUjCpoG
                20f0a04COwfneQAGGwd5oa+T8yO5hzuy
                Db/XcxxmK01EpqOyuxINew==";

$iv = 'r7BXXKkLb8qrSNn05n0qiA==';
$pc = new WXBizDataCrypt($appid, $sessionKey);
$errCode = $pc->decryptData($encryptedData, $iv, $data );
if ($errCode == 0) {
    print($data . "\n");
} else {
    print($errCode . "\n");
}


```
结果
```json
{
"openId": "oGZUI0egBJY1zhBYw2KhdUfwVJJE",
"nickName": "Band",
"gender": 1,
"language": "zh_CN",
"city": "Guangzhou",
"province": "Guangdong",
"country": "CN",
"avatarUrl": "http://wx.qlogo.cn/mmopen/vi_32/aSKcBBPpibyKNicHNTMM0qJVh8Kjgiak2AHWr8MHM4WgMEm7GFhsf8OYrySdbvAMvTsw3mo8ibKicsnfN5pRjl1p8HQ/0",
"unionId": "ocMvos6NjeKLIBqg5Mr9QjxrP1FA",
"watermark": {
"timestamp": 1477314187,
"appid": "wx4f4bc4dec97d474b"
}
}
```
---
### PhpAnalysis 用法

```php
    use Harvey\Toolsbox\library\phpanalysis\PhpAnalysis;
    
  $teststr = "2010年1月，美国国际消费电子展 (CES)上，联想将展出一款基于ARM架构的新产品，这有可能是传统四大PC厂商首次推出的基于ARM架构的消费电子产品，也意味着在移动互联网和产业融合趋势下，传统的PC芯片霸主英特尔正在遭遇挑战。
11月12日，联想集团副总裁兼中国区总裁夏立向本报证实，联想基于ARM架构的新产品正在筹备中。
英特尔新闻发言人孟轶嘉表示，对第三方合作伙伴信息不便评论。";
        PhpAnalysis::$loadInit = false;
        $pri_dict = false;//预载全部词条
        $pa = new PhpAnalysis('utf-8', 'utf-8', $pri_dict);
        $pa->LoadDict();
        $pa->SetSource($teststr);
        $pa->differMax = true;//多元切分
        $pa->unitWord = true;//新词识别
        $do_fork = true;//岐义处理
        $pa->StartAnalysis($do_fork);
        $do_prop = false;//词性标注
        $okresult = $pa->GetFinallyResult(' ', $do_prop);
        $pa_foundWordStr = $pa->foundWordStr;
        dump('分词结果');
        dump($okresult);
        dump('自动识别词');
        dump($pa_foundWordStr);
```
---
### Snowflake 雪花算法


1. simple to use.

```php
use Harvey\Toolsbox\library\Godruoyi\Snowflake\Snowflake;

$snowflake = new Snowflake();
$result = $snowflake->id();
var_dump($result);
// 1537200202186752
```

2. Specify the data center ID and machine ID.

```php
$snowflake = new Snowflake($datacenterId, $workerId);

$snowflake->id();
```

3. Specify start time.

```php
$snowflake = new Snowflake;
$snowflake->setStartTimeStamp(strtotime('2019-09-09')*1000);

$snowflake->id();
```
----

### ApiAuthService Api签名验证
```php
// 请求方
$buildUrl = \Harvey\Toolsbox\library\ApiAuthService::buildUrl('http://example.com', ['ts' => time(), 'a' => 1]);
var_dump($buildUrl);

// 接收方
$gets = explode('&', parse_url($buildUrl)['query']);
$params = [];
foreach ($gets as $get) {
    $arr = explode('=', $get);
    $params[$arr[0]] = $arr[1];
}
var_dump($params);
$result = \Harvey\Toolsbox\library\ApiAuthService::check($params);
var_dump('校验结果');
var_dump($result);
```
