<?php
require_once '../../vendor/autoload.php';
// 请求方
$buildUrl = \Harvey\Toolsbox\library\ApiAuthService::buildUrl('http://example.com', ['ts' => time(), 'a' => 1]);
var_dump($buildUrl);

// 接收方
$gets = explode('&', parse_url($buildUrl)['query']);
$params = [];
foreach ($gets as $get) {
    $arr = explode('=', $get);
    $params[$arr[0]] = $arr[1];
}
var_dump($params);
$result = \Harvey\Toolsbox\library\ApiAuthService::check($params);
var_dump('校验结果');
var_dump($result);